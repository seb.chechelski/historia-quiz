from django.db import models

# Create your models here.


class Przywodcy(models.Model):
    przywodca = models.CharField(max_length=50)
    start_panowania = models.DateField()
    koniec_panowania = models.DateField()
    lata_panowania = models.IntegerField()
    zdjecie = models.CharField(max_length=10000)

    def __str__(self):
        return f'{self.przywodca}'


class Wydarzenia(models.Model):
    wydarzenie = models.CharField(max_length=100)
    rok = models.IntegerField()
    przywodca = models.ForeignKey(Przywodcy, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.wydarzenie}'
