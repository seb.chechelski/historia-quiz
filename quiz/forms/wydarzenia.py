from django import forms
from quiz.models import Wydarzenia

class WydarzenieCreateForm(forms.ModelForm):
    class Meta:
        model = Wydarzenia
        fields = '__all__'

    rok = forms.IntegerField(min_value=0, max_value=2022)
    