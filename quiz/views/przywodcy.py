from django.views.generic import View
from django.shortcuts import render
from django.views.generic import FormView

from quiz.models import Przywodcy


class PrzywodcaReadView(View):
    def get(self, req):
        return render(req, 'przywodca_read.html', context={'dane': Przywodcy.objects.all()})
