from django.views.generic import View
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import FormView

from quiz.models import Wydarzenia
from quiz.forms.wydarzenia import WydarzenieCreateForm


class WydarzenieReadView(View):
    def get(self, req):
        return render(req, 'wydarzenie_read.html', context={'dane': Wydarzenia.objects.all()})


class WydarzenieCreateView(FormView):
    template_name = 'form.html'
    success_url = reverse_lazy("task")
    form_class = WydarzenieCreateForm

    def form_valid(self, form):
        result = super().form_valid(form)
        data = form.cleaned_data
        Wydarzenia.objects.create(
            rok=data['rok'],
        )
        return result